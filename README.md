# smart-village-api 

### Installation 
1. Clone this repo
    > ```git clone <url>```

2. Change Directory
    > ```cd smart-village-api```

3. Install Dependency
    > ```composer install```

3. Setup ENV Variable 
    > ```cp .env.example .env```
    >
    > open .env file and edit the variable

4. Run Database Migration
    > ```php artisan migrate```

### Run Project
```php -S <url>:<port> -t public```

### Run Test
```./vendor/bin/phpunit```
